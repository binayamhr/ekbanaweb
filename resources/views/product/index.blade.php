@extends('product.layouts.app')
@section('content')

        <div class="container">
        <div class="wrapper">

            <section class="content">

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Product Table</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>name</th>
                                    <th>description</th>
                                    <th>image</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($product as $data)
                                <tr role="row" >

                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$data->name}}</td>
                                    <td>{{$data->description}}</td>
                                    <td>
                                        <img src="/assets/image/{{$data->image}}" width="200px" height="200px">
                                    </td>
                                    <td>
                                        <a href="/products/{{$data->id}}/edit"><input type="button" value="edit" class="btn btn-info"></a>
                                        {{-- <a href="/products/{{$data->id}}"><input type="button" value="view" class="btn btn-warning" ></a> --}}
                                        <form method="post" action="/products/{{$data->id}}">
                                            @csrf
                                            {{-- @method('DELETE') --}}
                                        <input type="submit" value="delete" class="btn btn-danger">
                                        </form>
                                    </td>

                                </tr>
                                    @endforeach


                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </section>
        </div>
        </div>
    @endsection
