@extends('product.layouts.app');
@section('content')
    <div class="container">
        <div class="card card-primary">

            <div class="card-header">
                <h3 class="card-title">Update Product</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            @if (\Session::has('success'))
            <div class="alert alert-success">
                <ul>
                    <li>{!! \Session::get('success') !!}</li>
                </ul>
            </div>
        @endif
        <form role="form" method="post" action="/products/{{$product->id}}" enctype="multipart/form-data">
                @csrf
                {{-- @method('put') --}}
                <div class="card-body">
                    <div class="form-group">
                        <label for="product_name">Product name</label>
                    <input type="text" class="form-control" id="product_name" name="product_name" value="{{$product->name}}" placeholder="Enter product name">
                    </div>
                    <div class="form-group">
                        <label for="product_desc">Product description</label>
                        <input type="text" class="form-control" id="product_desc" name="product_desc" value="{{$product->description}}" placeholder="Enter product description">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">File input</label>
                        <div class="input-group">
                            <div class="custom-file">
                            <input type="file" class="custom-file-input" id="exampleInputFile" name="image">
                                <label class="custom-file-label" for="exampleInputFile">{{$product->image}}</label>
                            </div>
                            <div class="input-group-append">
                                <span class="input-group-text" id="">Upload</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Check me out</label>
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>

@endsection

@push('scripts')
    <script src="{{ asset('../../plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <script src="{{ asset('../../dist/js/adminlte.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            bsCustomFileInput.init();
        });
@endpush
