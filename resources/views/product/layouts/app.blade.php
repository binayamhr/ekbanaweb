
@include('product.partials.header')
@stack('styles')
@yield('content')
@include('product.partials.footer')
@stack('scripts')
