<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function showAdminRegisterForm()
    {
        return view('admin.register');
    }

    protected function createAdmin(Request $request)
    {

        //$this->validator($request->all())->validate();
        $this->validate($request, [
            'name' => 'required|string|min:5',
            'email' => 'required|string|email|max:255',
            'password' => 'required|max:6|confirmed',
        ]);
        // Validator::make($request->all(), [
        //     'name' => 'required|string|min:5',
        //     'email' => 'required|string|email|max:255',
        //     'password' => 'required|
        //      max:6|unique:regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/|confirmed',
            // 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/',
            // 'confirmed'],


        $admin = Admin::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        return redirect()->intended('login/admin');
    }


    public function showAdminLoginForm()
    {

        if (Auth::guard('admin')->check()) {
            return redirect('/admin');
        }

        return view('admin.login');
    }

    public function adminLogin(Request $request)

    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect('/admin');
        }
        return back()->withInput($request->only('email', 'remember'));
    }
    public function adminLogout()
    {
        Auth::guard('admin')->logout();
        return redirect('/login/admin');
    }
}
