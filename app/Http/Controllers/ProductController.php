<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::orderBy('id', 'desc')->limit(5)->get();
        return view('product.index', compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $image = $request['image'];
        $name = time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/assets/images');
        $image->move($destinationPath, $name);

        Product::create([
            'name' => $request['product_name'],
            'description' => $request['product_desc'],
            'image' => $name

        ]);
        return redirect()->back()->with('success', 'success creating product');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $product = Product::find($id);

        return view('product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $product = Product::find($id);

        $image_name = "";
        if ($request->hasFile($product->image)) {
            $path = public_path("/assets/images/" . $product->image);
            unlink($path);

            //update new image
            $image = $request->image;
            $name = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/assets/images');
            $image->move($destinationPath, $name);
            $image_name = $name;
        } else {

            $image_name = $product->image;
        }


        $product->name = $request->product_name;
        $product->description = $request->product_desc;
        $product->image = $image_name;
        $product->save();
        return redirect('/product')->with('success', ' product updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $id)
    {

        $product = Product::find($id);
        $path = public_path("/assets/images/" . $product->image);
        unlink($path);
        $product->delete();
    }
}
