<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Writer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class WriterController extends Controller
{

        public function showWriterRegisterForm()
    {
        return view('writer.register');
    }
    protected function createWriter(Request $request)
    {
        // $this->validator($request->all())->validate();
        $writer = Writer::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        return redirect()->intended('login/writer');
    }
    public function showWriterLoginForm()
    {

        if(Auth::guard('writer')->check()){
            return redirect('/writer');
        }
        return view('writer.login');
    }

    public function writerLogin(Request $request)
    {
         $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('writer')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect('/writer');
        }
        return back()->withInput($request->only('email', 'remember'));
    }
    public function writerLogout(){
        Auth::guard('writer')->logout();

        return redirect('/login/writer');
    }
}
