<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\TwoFactorController;
use App\Mail\TestMail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes(['verify'=>'true']);
 Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware([ 'auth','twofactor']);

Route::get('verify/resend', [TwoFactorController::class, 'resend'])->name('verify.resend');

Route::resource('verify', TwoFactorController::class)->only([
    'index', 'store'
]);



Route::get('/register/admin', [App\Http\Controllers\AdminController::class, 'showAdminRegisterForm']);
Route::post('/register/admin', [App\Http\Controllers\AdminController::class, 'createAdmin']);
Route::get('/login/admin', [App\Http\Controllers\AdminController::class, 'showAdminLoginForm'])->name('login.admin');
Route::post('/login/admin', [App\Http\Controllers\AdminController::class, 'adminLogin']);
Route::view('/admin', 'admin.home')->middleware('admin');
Route::get('/logout/admin', [App\Http\Controllers\AdminController::class, 'adminLogout'])->name('admin.logout');


Route::get('/register/writer', [App\Http\Controllers\WriterController::class, 'showWriterRegisterForm']);
Route::post('/register/writer', [App\Http\Controllers\WriterController::class, 'createWriter']);
Route::get('/login/writer', [App\Http\Controllers\WriterController::class, 'showWriterLoginForm'])->name('login.writer');
Route::post('/login/writer', [App\Http\Controllers\WriterController::class, 'writerLogin']);
Route::view('/writer', 'writer.home')->middleware('writer');

Route::get('/logout/writer', [App\Http\Controllers\WriterController::class, 'writerLogout'])->name('writer.logout');

//product section
Route::get('/product', [App\Http\Controllers\ProductController::class, 'index']);
Route::get('/product/create', [App\Http\Controllers\ProductController::class, 'create']);
Route::post('/product/store', [App\Http\Controllers\ProductController::class, 'store'])->name('product.store');
Route::get('/products/{id}/edit', [App\Http\Controllers\ProductController::class, 'edit']);
Route::post('/products/{id}', [App\Http\Controllers\ProductController::class, 'update']);
Route::post('/product/{id}', [App\Http\Controllers\ProductController::class, 'destroy']);

